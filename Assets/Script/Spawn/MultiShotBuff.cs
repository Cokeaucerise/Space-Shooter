﻿using UnityEngine;

public class MultiShotBuff : MonoBehaviour {

    public float tilt;
    public float speed;

    // Use this for initialization
    void Start() {
        GetComponent<Rigidbody>().angularVelocity = Random.insideUnitSphere * tilt;
        GetComponent<Rigidbody>().velocity = Vector3.forward * speed;
    }

    // Update is called once per frame
    void Update() {

    }

    public void OnTriggerEnter(Collider collider) {
        if (collider.CompareTag("Player")) {
            EventManager.Publish(new PlayerBuff{ name = "MultiShot" });
            Destroy(gameObject);
        }
    }
}
