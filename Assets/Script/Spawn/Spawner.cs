﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour
{
    public float startDelay;
    public float waveRate;
    public int waveSize;
    public Vector2 spawnScale;
    public MinMax waveSizeFluctuation;
    public BoxCollider box;
    public Hazard[] spawns;
    public float destroyOnDeathDelay;

    private float startSpawning = 0;
    private Coroutine spawningCoroutine;

    // Use this for initialization
    void Start()
    {
        this.Subscribe();
        spawningCoroutine = StartCoroutine(SpawnHazards());
    }

    // Update is called once per frame
    void Update()
    {
    }

    private IEnumerator SpawnHazards()
    {
        float nextSpawn = 0;
        bool isSpawning = true;
        bool firstWave = false;

        startSpawning = Time.time + startDelay;
        while (isSpawning)
        {
            while (Time.time > startSpawning)
            {
                if (Time.time > nextSpawn)
                {
                    for (int i = 0; i < waveSize + Random.Range(waveSizeFluctuation.min, waveSizeFluctuation.max); i++)
                    {
                        Vector3 randomSpawn = new Vector3(
                            Random.Range(box.bounds.min.x, box.bounds.max.x),
                            Random.Range(box.bounds.min.y, box.bounds.max.y),
                            Random.Range(box.bounds.min.z, box.bounds.max.z)
                            );

                        Hazard spawn = Instantiate(spawns[Random.Range(0, spawns.Length)], randomSpawn, Quaternion.identity);
                        spawn.transform.localScale *= Random.Range(spawnScale.x, spawnScale.y);
                    }
                    firstWave = true;
                    nextSpawn = Time.time + waveRate;

                    if (waveRate <= 0 && firstWave)
                        break;
                }
                if (waveRate <= 0 && firstWave)
                    break;

                yield return new WaitForSeconds(1);
            }

            if (waveRate <= 0 && firstWave)
                break;

            yield return new WaitForSeconds(0.1f);
        }
    }

    [Subscribe(typeof(PlayerStateChange))]
    public void OnPlayerStateChange(PlayerStateChange value)
    {
        if (!value.Alive)
        {
            StopCoroutine(spawningCoroutine);
            foreach (Hazard hazard in FindObjectsOfType<Hazard>())
            {
                Destroy(hazard.gameObject, destroyOnDeathDelay);
            }
        }
        else
        {
            spawningCoroutine = StartCoroutine(SpawnHazards());
        }
    }
}
