﻿using UnityEngine;

public class Asteroid : Hazard
{

    public float speed;
    public float rotation;
    public GameObject explosion;
    public MinMax subSpawnQuantityRange;
    public MinMax subSpawnScale;
    public GameObject[] subSpawns;
    public Vector3 minSubSpawnSize;

    // Use this for initialization
    void Start()
    {
        this.Subscribe();

        GetComponent<Rigidbody>().velocity = GetComponent<Rigidbody>().velocity + speed * Random.insideUnitSphere;
        GetComponent<Rigidbody>().angularVelocity = Random.insideUnitSphere * rotation * Random.value;
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void OnTriggerEnter(Collider collider)
    {
        if (collider.CompareTag("Shot") || collider.CompareTag("Player") ||
            collider.CompareTag("EnemyShot") || collider.CompareTag("EnemyShip"))
        {
            Explode();
            if (collider.CompareTag("Shot"))
                EventManager.Publish(new ScoreChange { newScorePoints = gameObject.transform.localScale.magnitude });
        }
    }


    private void Explode()
    {
        Destroy(gameObject);
        GameObject exp = Instantiate(explosion, transform.position, Quaternion.identity);
        exp.transform.localScale *= gameObject.transform.localScale.magnitude;
        Destroy(exp, 2);

        for (int i = 0; i < Random.Range((int)subSpawnQuantityRange.min, (int)subSpawnQuantityRange.max); i++)
        {
            Vector3 newScale = gameObject.transform.localScale * subSpawnScale.RandomInRange;

            if (newScale.magnitude > minSubSpawnSize.magnitude)
            {

                GameObject subSpawn = subSpawns[Random.Range(0, subSpawns.Length)];
                GameObject instance = ((GameObject)Instantiate(subSpawn, transform.position, Quaternion.identity));

                instance.GetComponent<Rigidbody>().transform.localScale = newScale;

                Vector3 angleVelocity =
                    new Vector3(
                        Random.Range(0.5f, 1f) * (Random.value >= 0.5 ? 1 : -1),
                        Random.Range(0.5f, 1f) * (Random.value >= 0.5 ? 1 : -1),
                        GetComponent<Rigidbody>().velocity.magnitude * Random.Range(0.5f, 1f)
                    );

                instance.GetComponent<Rigidbody>().velocity = angleVelocity;
                instance.GetComponent<Rigidbody>().angularVelocity = Random.insideUnitSphere * rotation * Random.value;
            }
        }
    }
}
