﻿using UnityEngine;
using System.Linq;

public class Shot : MonoBehaviour
{

    public float speed;
    public float lifetime = 0;
    public GameObject[] destroyedOnContact;

    // Use this for initialization
    void Start()
    {
        Vector3 shotVelocity = gameObject.transform.rotation * (Vector3.up * speed);
        Vector3 initVelocity = gameObject.GetComponent<Rigidbody>().velocity;
        GetComponent<Rigidbody>().velocity = new Vector3
        {
            x = initVelocity.x + shotVelocity.x,
            y = initVelocity.y + shotVelocity.y,
            z = initVelocity.z + shotVelocity.z
        };
        if (lifetime > 0)
            Destroy(gameObject, lifetime);
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void OnTriggerEnter(Collider collider)
    {
        if (destroyedOnContact.Any(x => collider.CompareTag(x.tag)))
            Destroy(gameObject);
    }
}
