﻿using UnityEngine;
using System.Collections;

public class EnemyShip : Hazard
{

    public GameObject explosion;
    public float points;

    public float speed;
    public float tilt;
    public float smoothing;
    public Limit limit;
    public bool aimPlayer;

    public float shotRate;
    public float shotNumber;
    public GameObject shot;
    public GameObject shotSpawn;

    public float rollSmoothing;
    public Vector2 rollRange;
    public float rollRate;
    public float firstRoll;

    private bool barrelroll;

    // Use this for initialization
    void Start() {
        this.Subscribe();

        GetComponent<Rigidbody>().velocity = new Vector3(0, 0, speed) * -1;

        StartCoroutine(Shoot(shot, shotSpawn, shotRate));
        StartCoroutine(BarrelrollEnumarator(gameObject, rollSmoothing, rollRange, rollRate, firstRoll));
    }
	
    // Update is called once per frame
    void Update() {

        if (barrelroll)
            return;

        Rigidbody body = GetComponent<Rigidbody>();

        if (aimPlayer)
            FollowPlayer();

        body.rotation = Quaternion.Euler(body.rotation.x, 180, body.velocity.x * tilt);

        body.position = new Vector3(
            Mathf.Clamp(body.position.x, limit.x.min, limit.x.max),
            0f,
            Mathf.Clamp(body.position.z, limit.z.min, limit.z.max)
        );

    }

    public void OnTriggerEnter(Collider collider) {
        if (collider.CompareTag("Player") || collider.CompareTag("Shot")) {
            Destroy(gameObject);
            Destroy(Instantiate(explosion, gameObject.transform.position, Quaternion.identity), 2);
            EventManager.Publish(new ScoreChange{ newScorePoints = points });
        }
    }

    private void FollowPlayer() {

        Rigidbody body = GetComponent<Rigidbody>();
        GameObject playerShip = GameObject.FindGameObjectWithTag("Player");

        if (playerShip == null) {
            body.velocity = Vector3.forward;
        }
        else if (playerShip.gameObject != null) {

            body.velocity = new Vector3(
                Mathf.MoveTowards(body.velocity.x, playerShip.transform.position.x - body.transform.position.x, smoothing),
                0f,
                body.velocity.z
            );
        }
    }

    private static IEnumerator Shoot(GameObject shotInstance, GameObject spawn, float shotRate, float startWait = 0) {

        yield return new WaitForSeconds(startWait);

        while (true) {
            Instantiate(shotInstance, spawn.transform.position, Quaternion.Euler(90, 0, 0));
            yield return new WaitForSeconds(shotRate);
        }

    }

    private IEnumerator BarrelrollEnumarator(GameObject ship, float smoothing, Vector2 rollRange, float rollRate, float firstRoll) {

        float nextRoll = Time.time + firstRoll;

        while (true) {

            if (Time.time > nextRoll && !barrelroll) {
                StartCoroutine(Barrelroll(gameObject, rollSmoothing, rollRange));
                nextRoll = Time.time + rollRate;
            }
            yield return new WaitForSeconds(1);
        }
    }

    private IEnumerator Barrelroll(GameObject ship, float smoothing, Vector2 rollRange) {

        barrelroll = true;
        Rigidbody body = ship.GetComponent<Rigidbody>();
        float initialPosition = body.position.x;
        float rollDistance = Random.Range(rollRange.x, rollRange.y) * -(Mathf.Sign(body.position.x));

        while (barrelroll) {
            
            float targetX = Mathf.MoveTowards(body.velocity.x, rollDistance, smoothing);

            body.velocity = new Vector3(
                targetX,
                0f,
                body.velocity.z
            );

            float rollProgress = (body.position.x - initialPosition) / rollDistance;
            float rollRotation = 360 * rollProgress * Mathf.Sign(targetX);

            body.rotation = Quaternion.Euler(body.rotation.x, 180, rollRotation);

            if (Mathf.Abs(rollRotation) >= 360) {
                barrelroll = false;
                body.velocity = new Vector3(
                    0f,
                    0f,
                    body.velocity.z
                );
            }
                
            yield return new WaitForEndOfFrame();
        }
    }
}
