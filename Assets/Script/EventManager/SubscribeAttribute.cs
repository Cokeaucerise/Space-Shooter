﻿using System;

[AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
public class SubscribeAttribute : Attribute
{
    public Type eventType;

    public SubscribeAttribute(Type eventType) {
        this.eventType = eventType;
    }
}
