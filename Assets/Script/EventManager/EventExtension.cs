﻿using UnityEngine;

public static class EventExtension
{

    public static void Subscribe(this MonoBehaviour instance) {
        EventManager.Subscribe(instance);
    }

    public static void Unsubscribe(this MonoBehaviour instance) {
        EventManager.Unsubscribe(instance);
    }
}