﻿using System;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;

public static class EventManager
{

    private static IDictionary<Type, IEnumerable<Delegate>> _eventSubscriptions;

    private static IDictionary<Type, IEnumerable<Delegate>> eventSubscriptions
    {
        get
        {         
            if (_eventSubscriptions == null)
                _eventSubscriptions = new Dictionary<Type, IEnumerable<Delegate>>();
            return _eventSubscriptions;
        }

        set{ _eventSubscriptions = value; }
    }


    /// <summary>
    /// Subscribe the specified instance.
    /// Extract all the functons of the instance that has the SubscribeAttribute.
    /// The functions will be unsubscribes when the instance is deleted;  
    /// </summary>
    /// <param name="instance">Instance.</param>
    public static void Subscribe(System.Object instance) {
        IEnumerable<MethodInfo> methods = instance.GetType()
            .GetMethods()
            .Where(m => m.GetCustomAttributes(typeof(SubscribeAttribute), false).Length > 0);

        foreach (MethodInfo method in methods) {
            
            Type eventType = GetEventType(method);
            Type actionType = typeof(Action<>).MakeGenericType(eventType);

            Subscribe(eventType, Delegate.CreateDelegate(actionType, instance, method));
        }
    }

    /// <summary>
    /// Subscribe the specified delegate(Function) to a event.
    /// </summary>
    /// <param name="eventType">Event type.</param>
    /// <param name="deleg">Deleg.</param>
    public static void Subscribe(Type eventType, Delegate deleg) {

        if (!eventSubscriptions.ContainsKey(eventType))
            eventSubscriptions.Add(new KeyValuePair<Type, IEnumerable<Delegate>>(eventType, new List<Delegate>()));

        ((List<Delegate>)eventSubscriptions[eventType]).Add(deleg);
    }


    /// <summary>
    /// Unsubscribe the specified instance.
    /// Manually remove all the subcriptions functions of the instances
    /// </summary>
    /// <param name="instance">Instance.</param>
    public static void Unsubscribe(System.Object instance) {
        foreach (KeyValuePair<Type, IEnumerable<Delegate>> subscriptions in eventSubscriptions)
            ((List<Delegate>)subscriptions.Value).RemoveAll(x => x.Target.Equals(instance));
    }

    private static Type GetEventType(MethodInfo method) {
        return ((SubscribeAttribute)method
            .GetCustomAttributes(typeof(SubscribeAttribute), false)
            .First())
            .eventType;
    }

    public static void Publish(System.Object publish) {

        if(!eventSubscriptions.ContainsKey(publish.GetType()))
            return;

        for (int i = 0; i < eventSubscriptions[publish.GetType()].Count(); i++) {
            Delegate del = eventSubscriptions[publish.GetType()].ElementAt(i);

            if (del.Target.Equals(null))
                ((List<Delegate>)eventSubscriptions[publish.GetType()]).RemoveAt(i);

            del.DynamicInvoke(publish);
        }
    }
}