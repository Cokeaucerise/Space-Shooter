﻿using System.Collections;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public float speed;
    public float tilt;
    public float cameraSmoothing = 1f;
    public float decaleration = 0.1f;

    public float invincibilityDuration
    ;
    public float shotRate;
    public float shotNumber;
    public MinMax shotNumberLimit;
    public float shotAngleIncrement;
    public GameObject shotSpawn;
    public GameObject shot;
    public Limit limit;
    public GameObject explosion;

    private float nextShot;
    private bool isInvincible;
    private Rigidbody body;

    // Use this for initialization
    void Start()
    {
        this.Subscribe();
        body = GetComponent<Rigidbody>();

        StartCoroutine(velocityCoroutine());
        StartCoroutine(rotationCoroutine());
    }

    // Update is called once per frame
    void Update()
    {
        if ((Input.GetButton("Jump") || Input.GetButton("Fire1")) && !isInvincible)
            Shoot();
    }

    private IEnumerator velocityCoroutine()
    {
        while (true)
        {
            float y = Input.GetAxis("Vertical");
            Vector3 direction = transform.forward * y * speed;

            body.velocity = direction;

            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator rotationCoroutine()
    {
        while (true)
        {
            Vector3 direction = new Vector3
            {
                x = -(Input.mousePosition.y - Screen.height / 2) / (Screen.height / 2),
                y = (Input.mousePosition.x - Screen.width / 2) / (Screen.width / 2),
                z = Mathf.Lerp(body.angularVelocity.z, -Input.GetAxis("Horizontal"), 1)
            };

            body.angularVelocity = body.rotation * direction * cameraSmoothing;

            yield return new WaitForEndOfFrame();
        }
    }

    private void toggleInvincibility()
    {
        isInvincible = !isInvincible;
    }

    private IEnumerator Invincibility(float start, float end)
    {
        toggleInvincibility();
        while (Time.time >= start && Time.time < end)
        {
            GetComponentInChildren<MeshRenderer>().enabled = !GetComponentInChildren<MeshRenderer>().enabled;
            float x = end - Time.time;
            float y = Mathf.Pow(x / 15, 2) + 0.1f;
            yield return new WaitForSeconds(y);
        }
        ;
        GetComponentInChildren<MeshRenderer>().enabled = true;
        toggleInvincibility();
    }

    private void Shoot()
    {
        if (Time.time >= nextShot)
        {
            nextShot = Time.time + shotRate;
            CreateShot(shot, shotSpawn.transform.position, gameObject.transform.rotation * shot.transform.rotation);
        }
    }

    private void CreateShot(GameObject shotInstance, Vector3 position, Quaternion rotation)
    {
        GameObject angleShot = ((GameObject)Instantiate(shotInstance, position, rotation));
        angleShot.GetComponent<Rigidbody>().velocity = gameObject.GetComponent<Rigidbody>().velocity;
    }

    private void Explode()
    {
        Destroy(gameObject);
        Destroy(Instantiate(explosion, transform.position, Quaternion.identity), 2);
    }

    public void OnTriggerEnter(Collider collider)
    {
        if ((collider.CompareTag("Hazard") || collider.CompareTag("EnemyShip") || collider.CompareTag("EnemyShot")) && !isInvincible)
        {
            StartCoroutine(Invincibility(Time.time, Time.time + invincibilityDuration));
            EventManager.Publish(new PlayerDamage { Damage = collider.gameObject.transform.localScale.magnitude });
        }
    }

    [Subscribe(typeof(PlayerStateChange))]
    public void OnPlayerStateChange(PlayerStateChange value)
    {
        if (!value.Alive)
            Explode();
    }

    [Subscribe(typeof(PlayerBuff))]
    public void OnPlayerBuff(PlayerBuff value)
    {
        if (value.name == "MultiShot")
        {
            shotNumber += 2;
            shotNumber = Mathf.Clamp(shotNumber, shotNumberLimit.min, shotNumberLimit.max);
        }
    }
}
