﻿using UnityEngine;
using System;

[Serializable]
public class PlayerSpawn : MonoBehaviour
{

    public Vector3 spawn;
    public GameObject playerShip;

    private GameObject currentPlayerShip;    

    // Use this for initialization
    void Start() {
        this.Subscribe();
        currentPlayerShip = (GameObject)Instantiate(playerShip, spawn, Quaternion.identity);
    }
	
    // Update is called once per frame
    void Update() {
        
    }

    public GameObject CurrentPlayerShip(){
        return currentPlayerShip;
    }

    [Subscribe(typeof(PlayerStateChange))]
    public void OnPlayerStateChange(PlayerStateChange value) {
        if (value.Alive == true)
            currentPlayerShip = (GameObject)Instantiate(playerShip, spawn, Quaternion.identity);
    }
}
