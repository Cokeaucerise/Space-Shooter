﻿using UnityEngine;

public class BoundaryBox : MonoBehaviour
{
    private BoxCollider boxCollider;

    // Use this for initialization
    void Start()
    {
        boxCollider = GetComponent<BoxCollider>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnTriggerExit(Collider collider)
    {
        // Get the object cuurrent position
        Vector3 colliderPosition = collider.gameObject.transform.position;
        // Invert all the position because the object leaved the bounds of the Box
        Vector3 newPosition = new Vector3
        {
            x = boxCollider.bounds.min.x + (boxCollider.bounds.max.x - colliderPosition.x),
            y = boxCollider.bounds.min.y + (boxCollider.bounds.max.y - colliderPosition.y),
            z = boxCollider.bounds.min.z + (boxCollider.bounds.max.z - colliderPosition.z)
        };

        // Set the new inverted positions to the parent object of the collider
        collider.gameObject.transform.position = newPosition;
    }
}