﻿using UnityEngine;

public class Cursor : MonoBehaviour
{

    public Texture2D cursorTexture;
    public Vector2 cursorSize = Vector2.one;

    // Use this for initialization
    void Start()
    {
        cursorTexture.Resize((int)cursorSize.x, (int)cursorSize.y);
        UnityEngine.Cursor.SetCursor(cursorTexture, new Vector2(cursorTexture.width / 2, cursorTexture.height / 2), CursorMode.ForceSoftware);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
