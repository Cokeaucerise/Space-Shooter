﻿using UnityEngine;

public class Score : MonoBehaviour
{

    public float score;
    public GameObject scoreText;

    // Use this for initialization
    void Start() {
        this.Subscribe();
        scoreText.GetComponent<GUIText>().text = score.ToString("F1");
    }
	
    // Update is called once per frame
    void Update() {
        scoreText.GetComponent<GUIText>().text = score.ToString("F1");
    }

    [Subscribe(typeof(ScoreChange))]
    public void OnScoreChange(ScoreChange scoreChange) {
        score += scoreChange.newScorePoints;
    }

    [Subscribe(typeof(PlayerStateChange))]
    public void OnPlayerStateChange(PlayerStateChange value) {
        if (value.Alive)
            score = 0;
    }
}
