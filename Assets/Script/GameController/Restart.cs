﻿using UnityEngine;

public class Restart : MonoBehaviour
{

    public float cooldownOnDeath;

    private bool playerState = true;
    private float playerDeathCooldown;

    // Use this for initialization
    void Start() {
        this.Subscribe();
        GetComponent<GUIText>().enabled = false;
    }
	
    // Update is called once per frame
    void Update() {
        bool playerRespawnable = !playerState && Time.time > playerDeathCooldown;
        GetComponent<GUIText>().enabled = playerRespawnable;
        if (playerRespawnable && Input.GetKey(KeyCode.R))
            EventManager.Publish(new PlayerStateChange{ Alive = true });
    }

    [Subscribe(typeof(PlayerStateChange))]
    public void OnPlayerStateChange(PlayerStateChange value) {
        playerState = value.Alive;
        playerDeathCooldown = Time.time + cooldownOnDeath;
    }
}
