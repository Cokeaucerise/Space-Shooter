﻿using UnityEngine;
using System;

[Serializable]
public class Limit
{
    public MinMax x;
    public MinMax y;
    public MinMax z;

    public Vector3 RandomVector3
    {
        get
        {
            return new Vector3(
                UnityEngine.Random.Range(x.min, x.max),
                UnityEngine.Random.Range(y.min, y.max),
                UnityEngine.Random.Range(z.min, z.max)
            );
        }
    }
}

[Serializable]
public class MinMax
{
    public float min;
    public float max;

    public float RandomInRange
    {
        get
        {
            return UnityEngine.Random.Range(min, max);
        }
    }
}
