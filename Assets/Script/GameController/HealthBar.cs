﻿using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{

    // Use this for initialization
    void Start() {
        this.Subscribe();
    }
	
    // Update is called once per frame
    void Update() {
	
    }

    [Subscribe(typeof(PlayerDamage))]
    public void OnPlayerDamage(PlayerDamage value) {
        GetComponentInChildren<Slider>().value -= value.Damage;

        if (GetComponentInChildren<Slider>().value <= 0)
            EventManager.Publish(new PlayerStateChange{ Alive = false });
    }

    [Subscribe(typeof(PlayerStateChange))]
    public void OnPlayerStateChange(PlayerStateChange value) {
        if (!GetComponent<Canvas>().enabled && value.Alive)
            GetComponentInChildren<Slider>().value = GetComponentInChildren<Slider>().maxValue;
        
        GetComponent<Canvas>().enabled = value.Alive;
    }
}
