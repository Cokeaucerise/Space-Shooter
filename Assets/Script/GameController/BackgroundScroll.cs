﻿using UnityEngine;

public class BackgroundScroll : MonoBehaviour
{

    public float scrollSpeed;
    public float ZSize;

    private Vector3 initialPosition;

    // Use this for initialization
    void Start()
    {
        initialPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = initialPosition + (Mathf.Repeat(Time.time * scrollSpeed, ZSize) * Vector3.forward);
    }
}
