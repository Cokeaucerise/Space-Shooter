﻿using UnityEngine;

public class Timer : MonoBehaviour
{

    public float time;
    public float startTime;
    public bool timerActive;
    public GameObject timerText;

    // Use this for initialization
    void Start() {
        this.Subscribe();
    }

    // Update is called once per frame
    void Update() {
        float timer = timerActive ? Time.time - startTime : time;
        timerText.GetComponent<GUIText>().text = (timer / 60).ToString("00") + ":" + (timer % 60).ToString("00"); 
    }

    [Subscribe(typeof(PlayerStateChange))]
    public void OnPlayerStateChange(PlayerStateChange value) {
        timerActive = value.Alive;
        if (!value.Alive)
            time = Time.time - startTime;
        else
            startTime = Time.time;
    }
}
