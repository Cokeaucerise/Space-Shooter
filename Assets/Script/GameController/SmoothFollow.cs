﻿// Converted from UnityScript to C# at http://www.M2H.nl/files/js_to_c.php - by Mike Hergaarden

using UnityEngine;

// Place the script in the Camera-Control group in the component menu
[AddComponentMenu("Camera-Control/Smooth Follow CSharp")]

public class SmoothFollow : MonoBehaviour
{
    /*
     This camera smoothes out rotation around the y-axis and height.
     Horizontal Distance to the target is always fixed.
     
     There are many different ways to smooth the rotation but doing it this way gives you a lot of control over how the camera behaves.
     
     For every of those smoothed values we calculate the wanted value and the current value.
     Then we smooth it using the Lerp function.
     Then we apply the smoothed values to the transform's position.
     */

    // The target we are following
    public GameObject target;
    public float distance = 5f;
    public Vector3 distanceDirection = Vector3.back;
    public Vector3 localDistances = Vector3.up;
    public float damping = 3.0f;

    void LateUpdate()
    {
        // Early out if we don't have a target
        if (!target)
            return;

        // Calculate the current rotation angles
        Quaternion currentRotationAngle = transform.rotation;
        Quaternion wantedRotationAngles = target.transform.rotation;

        // Damp the rotation around the y-axis
        Quaternion currentRotation = Quaternion.Lerp(currentRotationAngle, wantedRotationAngles, damping * Time.deltaTime);

        // Set the position of the camera
        transform.position = target.transform.position;
        // distance of the target
        transform.position += currentRotation * distanceDirection * distance;

        // Always have the top be above the player
        transform.LookAt(target.transform, target.transform.up * 1000);

        // Set local position of camera when at correct distance 
        transform.position += currentRotation * localDistances;
    }
}